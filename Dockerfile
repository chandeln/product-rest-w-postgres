FROM frolvlad/alpine-oraclejdk8:slim
VOLUME /tmp
ADD target/product-rest-w-postgres.war product-rest-w-postgres.war
ENV JAVA_OPTS=""
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /product-rest-w-postgres.war"]
