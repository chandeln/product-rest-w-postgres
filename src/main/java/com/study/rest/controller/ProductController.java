package com.study.rest.controller;

import com.study.rest.domain.Product;
import com.study.rest.exception.ResourceNotFoundException;
import com.study.rest.service.ProductService;
import java.net.URI;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * A very basic CRUD-based REST application for products using in-memory DB
 */
@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    private ProductService productService;

    //CREATE
    //URI: http://localhost:8080/api/products
    @RequestMapping(value = "/products", method = RequestMethod.POST)
    public ResponseEntity<Void> createProduct(@Valid @RequestBody Product product) {
        product = productService.saveProduct(product);
        //Set the location header for the newly created resource
        HttpHeaders responseHeaders = new HttpHeaders();
        URI newProductUri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(product.getProductId()).toUri();
        responseHeaders.setLocation(newProductUri);
        return new ResponseEntity<>(null, responseHeaders, HttpStatus.CREATED);
    }

    //READ ALL
    //URI: http://localhost:8080/api/products
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Product>> getAllProducts() {
        Iterable<Product> products = productService.getAllProducts();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    //READ ONE
    //URI: http://localhost:8080/api/products/1
    @RequestMapping(value = "/products/{productId}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable Long productId) {
        verifyProductExists(productId);
        Product product = productService.getProduct(productId);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    //UPDATE
    //URI: http://localhost:8080/api/products/1
    @RequestMapping(value = "/products/{productId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateProduct(@PathVariable Long productId, @Valid @RequestBody Product product) {
        verifyProductExists(productId);
        productService.saveProduct(product);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //DELETE
    //URI: http://localhost:8080/api/products/3
    @RequestMapping(value = "/products/{productId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteProduct(@PathVariable Long productId) {
        verifyProductExists(productId);
        productService.deleteProduct(productId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    protected void verifyProductExists(Long productId) throws ResourceNotFoundException {
        Product product = productService.getProduct(productId);
        if (product == null) {
            throw new ResourceNotFoundException("Product with id " + productId + " not found...");
        }
    }

}
