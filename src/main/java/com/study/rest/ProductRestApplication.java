package com.study.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/*
* A very basic Spring Boot based REST application that gets all products and single product from in-memory DB
 */
@SpringBootApplication
//@EnableHypermediaSupport(type = HypermediaType.HAL) //NOT SURE IF YOU NEED THIS FOR HATEOAS
public class ProductRestApplication extends SpringBootServletInitializer {

    /*
    For starting the application in both JAR and also deploying in standalone TC using WAR
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ProductRestApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(ProductRestApplication.class, args);
    }

}
